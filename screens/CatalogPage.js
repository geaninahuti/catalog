import React, {Component} from 'react';
import {StyleSheet, TextInput, ScrollView, View, Text, Alert} from 'react-native';
import Courses from './Courses';
import Profile from './Profile';
import Grades from './Grades';
import Attendances from './Attendances.js'
import * as firebase from "firebase";


import Tabbar from 'react-native-tabbar-bottom'

const config = {
    apiKey: "AIzaSyDLExTC3v_NByNGRMO310k4tKjoMPR5ekw",
    authDomain: "studentcatalog-cebe5.firebaseapp.com",
    databaseURL: "https://studentcatalog-cebe5.firebaseio.com",
    projectId: "studentcatalog-cebe5",
    storageBucket: "",
    messagingSenderId: "132643019625"
};

export default class UserPage extends Component {
    static navigationOptions = {
        header: null,
    }

    constructor(props) {
        super(props)
        this.state = {
            page: "GradesScreen",
        }
    }

    componentWillMount() {
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {
                    // if you are using react-navigation just pass the navigation object in your components like this:
                    // {this.state.page === "HomeScreen" && <MyComp navigation={this.props.navigation}>Screen1</MyComp>}
                }
                {this.state.page === "GradesScreen" && <Grades navigation={this.props.navigation}></Grades>}
                {this.state.page === "AttendancesScreen" && <Attendances navigation={this.props.navigation}></Attendances>}


                <Tabbar style={styles.tabbar}
                        stateFunc={(tab) => {
                            this.setState({page: tab.page})
                            //this.props.navigation.setParams({tabTitle: tab.title})
                        }}
                        tabbarBgColor='#263238'
                        activePage={this.state.page}
                        labelSize={15}
                        selectedLabelColor = 'white'
                        selectedIconColor = 'white'
                        iconColor = '#607D8B'
                        labelColor = '#607D8B'
                        tabs={[
                            {
                                page: "GradesScreen",
                                icon: "book",
                                iconText: "Note"
                            },
                            {
                                page: "AttendancesScreen",
                                icon: "checkmark",
                                iconText: "Prezente"
                            }
                        ]}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#607D8B',
        // backgroundColor:'yellow',
        flex: 1,

    },
    tabbar: {
        // marginTop:'0.01%'
    }
});