import * as React from 'react';
import {View, StyleSheet, Text, Alert, ScrollView, TouchableOpacity} from 'react-native';
import autobind from 'autobind-decorator';
import * as firebase from "firebase";
import renderIf from '../components/renderIf';
import {Table, TableWrapper, Row, Rows, Col, Cols, Cell} from 'react-native-table-component';

const config = {
    apiKey: "AIzaSyDLExTC3v_NByNGRMO310k4tKjoMPR5ekw",
    authDomain: "studentcatalog-cebe5.firebaseapp.com",
    databaseURL: "https://studentcatalog-cebe5.firebaseio.com",
    projectId: "studentcatalog-cebe5",
    storageBucket: "",
    messagingSenderId: "132643019625"
};

export default class Grades extends React.Component {
    static navigationOptions = {
        header: null,
    }
    state = {
        user: '',
        courseName: '',
        courseCode: '',
        labGrades: [],
        seminarGrades: [],
        courseGrades: [],
        tableHead: ['Numar', 'Data', 'Nota', 'Profesor'],
        tableHeadCurs: ['Numar', 'Data', 'Nota/Puncte bonus'],
        flexArrCurs: [1, 2, 3],
        widthArr: [1.2, 2, 1, 2, 4],
        tableLabData: [],
        tableSeminarData: [],
        tableCursData: [],
        msgError: false,
        selectedTab: 1,
    }

    constructor(props) {
        super(props);
        // this.setState({courseCode: this.props.navigation.getParam('courseCode', '0')});
        // this.setState({courseName: this.props.navigation.getParam('courseName', 'noName')});
        // if (!firebase.apps.length) {
        //     firebase.initializeApp(config);
        // }
        // this.setState({user: firebase.auth().currentUser.displayName});
        // this.getAllLabGrades();

    }

    @autobind
    componentDidMount() {
        this.setState({courseCode: this.props.navigation.getParam('courseCode', '0')});
        this.setState({courseName: this.props.navigation.getParam('courseName', 'noName')});
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        this.setState({user: firebase.auth().currentUser.displayName});
        this.getAllLabGrades();
        this.getAllSeminarGrades();
        this.getAllCourseGrades();
    }

    sortByProperty = function (property) {
        return function (x, y) {
            return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));
        };
    };

    @autobind
    async getAllLabGrades() {
        this.setState({msgError: true});
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];

        let ref = firebase.database().ref('grades');

        var childKey = username + this.props.navigation.getParam('courseCode', '0');

        ref.child(childKey).on("value", (entry) => {
            // returnDataTable = [];
            returnArrLab = [];

            try {
                // var labGrades = entry.val().laborator;
                // Alert.alert(labGrades.length.toString());

                var labGrades = entry.val().laborator;
                // Alert.alert(labAttendances.length.toString());
                for(var k in labGrades){
                    var v1 = labGrades[k].profesor;
                    var v2 = labGrades[k].date;
                    var v3 = labGrades[k].grade;
                    // var v4 = labGrades[k].penalizare;
                    var v5 = labGrades[k].nr;

                    var labGrade = [
                        v5,
                        v2,
                        v3,
                        // v4,
                        v1
                    ];
                    returnArrLab.push(labGrade);
                }
                // labGrades.forEach(function (childSnapshot) {
                //
                //     var v1 = childSnapshot.profesor;
                //     var v2 = childSnapshot.data;
                //     var v3 = childSnapshot.nota;
                //     var v4 = childSnapshot.penalizare;
                //     var v5 = childSnapshot.nr;
                //
                //     var labGrade = [
                //         v5,
                //         v2,
                //         v3,
                //         v4,
                //         v1
                //     ];
                //     returnArrLab.push(labGrade);
                // });

                // this.setState({labGrades: returnArr});
                // returnDataTable.push(returnArrLab);
                // returnArrSeminar = [];
                //
                // var seminarGrades = entry.val().grades.laborator;
                // // Alert.alert(labGrades.length.toString());
                // labGrades.forEach(function (childSnapshot) {
                //
                //     var v1 = childSnapshot.profesor;
                //     var v2 = childSnapshot.data;
                //     var v3 = childSnapshot.nota;
                //     var v4 = childSnapshot.penalizare;
                //     var v5 = childSnapshot.nr;
                //
                //     var seminarGrade = {
                //         professor: v1,
                //         date: v2,
                //         grade: v3,
                //         penalty: v4,
                //         number: v5
                //     };
                //     returnArrSeminar.push(seminarGrade);
                // });
                // returnDataTable.push(returnArrSeminar);
                this.setState({tableLabData: returnArrLab});
            } catch (e) {
                // Alert.alert("Nicio nota de afisat!")
            }
        })
        // Alert.alert(returnArrLab.length.toString());
    }

    @autobind
    async getAllSeminarGrades() {
        this.setState({msgError: true});
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];

        let ref = firebase.database().ref('grades');
        var childKey = username + this.props.navigation.getParam('courseCode', '0');

        ref.child(childKey).on("value", (entry) => {
            returnArrSeminar = [];
            try {
                var seminarGrades = entry.val().seminar;
                for(var k in seminarGrades){
                    var v1 = seminarGrades[k].profesor;
                    var v2 = seminarGrades[k].date;
                    var v3 = seminarGrades[k].grade;
                    var v5 = seminarGrades[k].nr;

                    var seminarGrade = [
                        v5,
                        v2,
                        v3,
                        v1
                    ];
                    returnArrSeminar.push(seminarGrade);
                }
                this.setState({tableSeminarData: returnArrSeminar});
            } catch (e) {
                // Alert.alert("Nicio nota de afisat!")
            }
        })
        // Alert.alert(returnArrLab.length.toString());
    }

    @autobind
    async getAllCourseGrades() {
        this.setState({msgError: true});
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];

        let ref = firebase.database().ref('grades');
        var childKey = username + this.props.navigation.getParam('courseCode', '0');

        ref.child(childKey).on("value", (entry) => {
            returnArrCurs = [];
            try {
                var cursGrades = entry.val().curs;
                for(var k in cursGrades){
                    var v2 = cursGrades[k].date;
                    var v3 = cursGrades[k].grade;
                    var v5 = cursGrades[k].nr;

                    var cursGrade = [
                        v5,
                        v2,
                        v3,
                    ];
                    returnArrCurs.push(cursGrade);
                }
                this.setState({tableCursData: returnArrCurs});
            } catch (e) {
                // Alert.alert("Nicio nota de afisat!")
            }
        })
        // Alert.alert(returnArrLab.length.toString());
    }

    @autobind
    deleteFromMyCourses() {
        var hasGrades = false;
        const {navigate} = this.props.navigation;
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];
        // console.log("Code" + this.state.code);
        let ref = firebase.database().ref('grades');
        var childKey = username + this.state.courseCode;

        ref.child(childKey).on("value", (entry) => {
            try {
                var labGrades = entry.val().laborator;
                // Alert.alert(labGrades.length.toString());
                if (labGrades.length > 0) {
                    hasGrades = true;
                }
            } catch (e) {
                // Alert.alert("Nu exista note la materia asta!")
            }
        });
        if (hasGrades) {
            Alert.alert(
                'Esti sigur ca vrei sa parasesti cursul?',
                'Exista note la aceasta materie.',
                [
                    {text: 'Cancel', style: 'cancel'},
                    {
                        text: 'OK', onPress: () => {
                            firebase.database().ref('enrolledStudents').child(childKey).remove();
                            navigate('UserPage')
                        }
                    },
                ],
            )
        }
        else {
            Alert.alert(
                'Esti sigur ca vrei sa parasesti cursul?',
                ' ',
                [
                    {text: 'Cancel', style: 'cancel'},
                    {
                        text: 'OK', onPress: () => {
                            firebase.database().ref('enrolledStudents').child(childKey).remove();
                            navigate('UserPage')
                        }
                    },
                ],
            )
        }
    }

    @autobind
    setTab(nr) {
        this.setState({selectedTab: nr});
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.mainContainer}>
                <View style={styles.nameContainer}>
                    <Text style={styles.nameStyle}>{this.state.courseName}</Text>
                </View>

                <View style={styles.gradesContainer}>
                    <View style={[styles.TOContainer, (this.state.selectedTab === 1) && {borderColor: 'white'}]}>
                        <TouchableOpacity
                            onPress={() => this.setTab(1)}
                        >
                            <Text
                                style={[styles.TOText, (this.state.selectedTab === 1) && {color: 'white'}]}>Laborator</Text>
                        </TouchableOpacity>
                    </View>

                    {/*<View style={styles.separatorContainer}>*/}
                        {/*<Text style={{fontSize: 20, color: '#607D8B'}}>|</Text>*/}

                    {/*</View>*/}

                    <View style={[styles.TOContainer, (this.state.selectedTab === 2) && {borderColor: 'white'}]}>
                        <TouchableOpacity
                            onPress={() => this.setTab(2)}
                        >
                            <Text
                                style={[styles.TOText, (this.state.selectedTab === 2) && {color: 'white'}]}>Seminar</Text>
                        </TouchableOpacity>
                    </View>

                    {/*<View style={styles.separatorContainer}>*/}
                        {/*<Text style={{fontSize: 20, color: '#607D8B'}}>|</Text>*/}

                    {/*</View>*/}

                    <View style={[styles.TOContainer, (this.state.selectedTab === 3) && {borderColor: 'white'}]}>
                        <TouchableOpacity
                            onPress={() => this.setTab(3)}
                        >
                            <Text
                                style={[styles.TOText, (this.state.selectedTab === 3) && {color: 'white'}]}>Curs</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/*<View style={styles.tableContainer}>*/}
                {/*<Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>*/}
                {/*<Row data={this.state.tableHead} flexArr={[1, 2, 1, 2, 2]} style={styles.head} textStyle={styles.textHead}/>*/}
                {/*<TableWrapper style={styles.wrapper}>*/}
                {/*<Rows data={this.state.tableLabData} flexArr={[1, 2, 1, 2, 2]}  textStyle={styles.text}/>*/}
                {/*</TableWrapper>*/}
                {/*</Table>*/}
                {/*</View>*/}
                {this.state.selectedTab !== 3 ?
                    <View style={styles.tableContainer}>
                        {/*<ScrollView>*/}
                            {/*<View>*/}
                                <Table borderStyle={{borderColor: '#C1C0B9', borderTopColor: 'black'}}>
                                    <Row data={this.state.tableHead} flexArr={this.state.widthArr}
                                         style={styles.header} textStyle={styles.textHeader}/>
                                </Table>
                                <ScrollView style={styles.dataWrapper}>
                                    {renderIf(this.state.selectedTab === 1,
                                        <Table borderStyle={{borderColor: '#C1C0B9'}} >
                                            {
                                                this.state.tableLabData.map((rowData, index) => (
                                                    <Row
                                                        key={index}
                                                        data={rowData}
                                                        flexArr={this.state.widthArr}
                                                        style={styles.row}
                                                        textStyle={styles.text}
                                                    />
                                                ))
                                            }
                                        </Table>)}
                                    {renderIf(this.state.selectedTab === 2,
                                        <Table borderStyle={{borderColor: '#C1C0B9'}}>
                                            {
                                                this.state.tableSeminarData.map((rowData, index) => (
                                                    <Row
                                                        key={index}
                                                        data={rowData}
                                                        flexArr={this.state.widthArr}
                                                        style={styles.row}
                                                        textStyle={styles.text}
                                                    />
                                                ))
                                            }
                                        </Table>)}
                                </ScrollView>
                            {/*</View>*/}
                        {/*</ScrollView>*/}
                    </View>
                    :
                    <View style={styles.tableContainerCurs}>
                        <ScrollView>
                            <View>
                                <Table borderStyle={{borderColor: 'white', borderTopColor: 'black'}}>
                                    <Row data={this.state.tableHeadCurs} flexArr={this.state.flexArrCurs}
                                         style={styles.header} textStyle={styles.textHeader}/>
                                </Table>
                                <ScrollView style={styles.dataWrapper}>
                                        <Table borderStyle={{borderColor: '#C1C0B9'}}>
                                            {
                                                this.state.tableCursData.map((rowData, index) => (
                                                    <Row
                                                        key={index}
                                                        data={rowData}
                                                        flexArr={this.state.flexArrCurs}
                                                        style={styles.row}
                                                        textStyle={styles.text}
                                                    />
                                                ))
                                            }
                                        </Table>
                                </ScrollView>
                            </View>
                        </ScrollView>
                    </View>
                }






                {/*<View style={styles.attendanceContainer}>*/}
                    {/*<View style={styles.TOContainer}>*/}
                        {/*<TouchableOpacity*/}
                            {/*onPress={() => navigate('UserPage')}*/}
                        {/*>*/}
                            {/*<Text style={styles.TOText}>Prezente</Text>*/}
                        {/*</TouchableOpacity>*/}
                    {/*</View>*/}
                {/*</View>*/}

                {/*<View style={styles.logOutContainer}>*/}
                    {/*<TouchableOpacity onPress={() => this.deleteFromMyCourses()}>*/}
                        {/*<Text style={styles.leaveCourseText}>Leave course</Text>*/}
                    {/*</TouchableOpacity>*/}
                {/*</View>*/}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        // flex: 0.93,
        flex:1,
        backgroundColor: '#607D8B',
        // backgroundColor:'blue'
    },
    nameStyle: {
        fontSize: 20,
        alignSelf: 'center',
        textAlign: 'center',
        color: 'white'
    },
    nameContainer: {
        marginTop: '2%',
        marginBottom: '2%',
        // justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor:'red'
        // flex:.0
    },
    logOutContainer: {
        width: '100%',
        height: '10%',
        justifyContent: 'center',
        alignItems: 'center',
        // position: 'absolute',
        // bottom: 0
        marginBottom:'10%'
    },
    gradesContainer: {
        margin: 5,
        // backgroundColor: '#263238',
        backgroundColor: '#607D8B',
        // backgroundColor:'red',
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'space-between',
        justifyContent: 'center',
        paddingLeft: '5%',
        paddingRight: '5%',
        // flex:.0
    },
    TOContainer: {
        // padding:2,
        // backgroundColor:'red',
        alignSelf:'center',
        borderColor:'#263238',
        // borderColor:'yellow',
        // borderWidth:0.5,
        borderBottomWidth:1,
        paddingLeft:5,
        paddingRight:5,
        marginLeft:'6%',
        marginRight:'6%',
    },
    TOText: {
        fontSize: 20,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: 'bold',
        lineHeight: 30,
        // color: '#607D8B',
        color: '#263238',

    },
    separatorContainer: {
        // backgroundColor:'white',
        // borderLeftColor:'white',
        // borderLeftWidth:1
    },
    // tableContainer:{
    //     padding: 16, paddingTop: 30, backgroundColor: 'red'
    // },
    // head: { height: 40, backgroundColor: '#263238' },
    // textHead:{
    //     color:'white',
    //     textAlign:'center'
    // },
    // text: { margin: 6,
    //     textAlign:'center'
    // },
    // wrapper: { flexDirection: 'row' },

    tableContainer: { flex:1, padding: '1%', paddingTop: '2%', backgroundColor: '#607D8B'},
    tableContainerCurs:{flex: 1, padding: '8%', paddingTop: '3%', backgroundColor: '#607D8B', paddingBottom: '2%'},
    header: {height: 50, backgroundColor: '#263238'},
    textHeader: {color: 'white', textAlign: 'center', fontWeight: 'bold', fontSize: 15},
    text: {textAlign: 'center', fontWeight: 'bold'},
    dataWrapper: {marginTop: -1, marginBottom:'10%'},
    row: {height: 50, backgroundColor: '#607D8B'},
    leaveCourseText: {
        fontSize: 15,
        fontWeight: 'bold',
        textDecorationLine: 'underline'
    },
    // attendanceContainer:{
    //     backgroundColor:'#263238',
    //     width:'40%',
    //     // justifyContent:'center',
    //     alignSelf:'center',
    //     marginTop:'2%'
    // }

});