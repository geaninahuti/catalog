import * as React from 'react';
import {View, StyleSheet, Text, Alert, ScrollView, TouchableOpacity, Image, TextInput, Picker} from 'react-native';
import Icon from 'react-native-fa-icons';
import autobind from 'autobind-decorator';
import * as firebase from "firebase";
import MyButton from '../components/MyButton';
import renderIf from '../components/renderIf.js'
import {KeyboardAvoidingView} from 'react-native';

const config = {
    apiKey: "AIzaSyDLExTC3v_NByNGRMO310k4tKjoMPR5ekw",
    authDomain: "studentcatalog-cebe5.firebaseapp.com",
    databaseURL: "https://studentcatalog-cebe5.firebaseio.com",
    projectId: "studentcatalog-cebe5",
    storageBucket: "",
    messagingSenderId: "132643019625"
};

export default class Courses extends React.Component {

    state = {
        user: '',
        editProfileTab: false,
        changePasswordTab: false,
        utliLinksTab: false,
        firstName: '',
        lastName: '',
        yearOfStudy: 0,
        group: 0,
        oldPassword: '',
        newPassword: '',
        cPassword: ''
    }

    constructor(props) {
        super(props);
    }

    async onPressLogout() {
        const {navigate} = this.props.navigation;
        try {
            await firebase.auth().signOut();
            // navigate('Loading');
        } catch (e) {
            console.log(e);
        }
    }

    @autobind
    componentWillMount() {
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        this.setState({user: firebase.auth().currentUser.displayName});
        this.getUserInfo();

    }

    @autobind
    changePassword() {
        // Ask signed in user for current password.
        const currentPass = this.state.oldPassword;
        const emailCred = firebase.auth.EmailAuthProvider.credential(
            firebase.auth().currentUser.email, currentPass);

        firebase.auth().currentUser.reauthenticateWithCredential(emailCred)
            .then(() => {
                // User successfully reauthenticated.
                if (this.state.newPassword.localeCompare(this.state.cPassword) === 0) {
                    const newPass = this.state.newPassword;
                    firebase.auth().currentUser.updatePassword(newPass);
                    Alert.alert("Succes!")
                    this.setState({changePasswordTab:false});
                }
                else
                {
                    Alert.alert("Passwords don't match!");
                }
            })
            .catch(error => {
                Alert.alert("Error! Incorrect old password!");
            });
    }

    @autobind
    openEditProfileTab() {
        this.setState({editProfileTab: !this.state.editProfileTab});
    }

    @autobind
    openChangePasswordTab() {
        this.setState({changePasswordTab: !this.state.changePasswordTab});
    }

    @autobind
    openUtilLinksTab() {
        this.setState({utliLinksTab: !this.state.utliLinksTab});
    }

    @autobind
    getUserInfo() {
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];

        let ref = firebase.database().ref('students');

        ref.child(username).on("value", (entry) => {
            try {
                var firstName = entry.val().firstName;
                var lastName = entry.val().lastName;
                var year = entry.val().year;
                var group = entry.val().group;
                // Alert.alert(labGrades.length.toString())

                this.setState({firstName: firstName});
                this.setState({lastName: lastName});
                this.setState({yearOfStudy: year});
                this.setState({group: group});


            } catch (e) {
                // Alert.alert("Nicio nota de afisat!")
            }
        })
        // Alert.alert(returnArrLab.length.toString());
    }


    @autobind
    editProfileSubmit() {
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];

        firebase.database().ref('students').child(username).update({
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            year: this.state.yearOfStudy,
            group: this.state.group
        });
        currentUser.updateProfile({
            displayName: this.state.firstName + " " + this.state.lastName,
        }).then(() => {
            this.setState({user: currentUser.displayName})
        });
    }

    render() {
        return (
            <View style={styles.mainContainer}>
            <ScrollView>
                <View style={styles.imageView}>
                    <Image source={require('./resources/student.png')} style={styles.imageStyle}/>
                </View>
                <View style={styles.nameContainer}>
                    <Text style={styles.nameStyle}>{this.state.user}</Text>
                </View>


                <TouchableOpacity>
                    <View style={styles.changeInfoContainer}>
                        <View style={styles.leftSide}>
                            <View>
                                <Text
                                    style={styles.textStyle}>
                                    Schimba parola
                                </Text>
                            </View>
                        </View>
                        <View style={styles.rightSide}>
                            <View>
                                <TouchableOpacity
                                    onPress={this.openChangePasswordTab}>
                                    <View>
                                        <Icon name='angle-down' style={{
                                            fontSize: 35,
                                            color: '#263238',
                                            paddingTop: 2,
                                            paddingBottom: 2,
                                            paddingLeft: 2,
                                            paddingRight: 2
                                        }}/>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>

                {renderIf(this.state.changePasswordTab,
                    <KeyboardAvoidingView style={styles.editProfileDetailsContainer}>
                        <View style={styles.textInputContainer}>
                            <Text>Parola veche</Text>
                            <TextInput
                                autoCorrect={false}
                                placeholderTextColor='white'
                                underlineColorAndroid='transparent'
                                style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}
                                onChangeText={(oldPassword) => this.setState({oldPassword})}
                                secureTextEntry={true}
                                // onChangeText={this.setLastName}
                                // ref={lastNameRef => this.lastNameRef = lastNameRef}
                                // onSubmitEditing={() => this.emailRef.focus()}
                            />
                        </View>
                        <View style={styles.textInputContainer}>
                            <Text>Parola noua</Text>
                            <TextInput
                                autoCorrect={false}
                                placeholderTextColor='white'
                                underlineColorAndroid='transparent'
                                style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}
                                secureTextEntry={true}
                                onChangeText={(newPassword) => this.setState({newPassword})}
                                // onChangeText={this.setLastName}
                                // ref={lastNameRef => this.lastNameRef = lastNameRef}
                                // onSubmitEditing={() => this.emailRef.focus()}
                            />
                        </View>
                        <View style={styles.textInputContainer}>
                            <Text>Confirma parola</Text>
                            <TextInput
                                autoCorrect={false}
                                placeholderTextColor='white'
                                underlineColorAndroid='transparent'
                                style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}
                                secureTextEntry={true}
                                onChangeText={(cPassword) => this.setState({cPassword})}
                                // onChangeText={this.setLastName}
                                // ref={lastNameRef => this.lastNameRef = lastNameRef}
                                // onSubmitEditing={() => this.emailRef.focus()}
                            />
                        </View>
                        <View style={styles.submitEditProfileContainer}>
                            <MyButton
                                text='Schimba parola'
                                onPress={() => this.changePassword()}
                            />
                        </View>
                    </KeyboardAvoidingView>)}

                {/*<TouchableOpacity>*/}
                    {/*<View style={styles.changeInfoContainer}>*/}
                        {/*<View style={styles.leftSide}>*/}
                            {/*<View>*/}
                                {/*<Text*/}
                                    {/*style={styles.textStyle}>*/}
                                    {/*Legaturi utile*/}
                                {/*</Text>*/}
                            {/*</View>*/}
                        {/*</View>*/}
                        {/*<View style={styles.rightSide}>*/}
                            {/*<View>*/}
                                {/*<TouchableOpacity*/}
                                    {/*onPress={this.openUtilLinksTab}>*/}
                                    {/*<View>*/}
                                        {/*<Icon name='angle-down' style={{*/}
                                            {/*fontSize: 35,*/}
                                            {/*color: '#263238',*/}
                                            {/*paddingTop: 2,*/}
                                            {/*paddingBottom: 2,*/}
                                            {/*paddingLeft: 2,*/}
                                            {/*paddingRight: 2*/}
                                        {/*}}/>*/}
                                    {/*</View>*/}
                                {/*</TouchableOpacity>*/}
                            {/*</View>*/}
                        {/*</View>*/}
                    {/*</View>*/}
                {/*</TouchableOpacity>*/}

                <View style={styles.logOutContainer}>
                    <MyButton
                        text='Sign out'
                        onPress={() => this.onPressLogout()}
                    />
                </View>
            </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 0.9
    },
    nameStyle: {
        fontSize: 30,
        alignSelf: 'center',
        color: 'white'
    },
    nameContainer: {
        // paddingTop: '10%',
        paddingBottom: '5%',
        // backgroundColor:'blue'
    },
    logOutContainer: {
        // width: '100%',
        // height: '20%',
        justifyContent: 'center',
        alignItems: 'center',
        // position: 'absolute',
        // bottom: 0,
        // backgroundColor:'yellow',
        width: '60%',
        alignSelf: 'center'
    },
    changeInfoContainer: {
        width: '90%',
        flexDirection: 'row',
        padding: 10,
        marginBottom: '2%',
        overflow: 'hidden',
        borderRadius: 1,
        backgroundColor: '#546E7A',
        alignSelf: 'center',
        // flex: .1
        // backgroundColor:'red'
    },
    textStyle: {
        fontSize: 25,
        alignSelf: 'center',
        fontWeight: 'bold'
    },

    leftSide: {
        flex: .9
    },
    rightSide: {
        justifyContent: 'center',
        flex: .1
    },
    imageView: {
        // paddingTop: 30,
        marginBottom: '1%',
        // height: '30%',
        // width: '40%',
        alignItems: 'center',
        alignSelf: 'center',
        // backgroundColor:'green'
        flex:.2
    },
    imageStyle: {
        //   padding: 5,
        // margin: '2%',
        // height: '40%',
        // width: '30%',
        resizeMode: 'stretch',
        // alignSelf: "center",
        tintColor: '#263238'
    },
    editProfileLineContainer: {
        flexDirection: 'row',
        // flex:1

    },
    textInputContainer: {
        // justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor: '#f2ccff',
        // backgroundColor: '#546E7A',
        // height: '50%',
        // borderRadius: 20,
        // margin: 10
        flex: .5,
        paddingLeft: '1%',
        margin: '1%',
        borderColor: 'black',
        borderWidth: 1
    },
    editProfileDetailsContainer: {
        width: '90%',
        // flexDirection: 'row',
        // padding: 10,
        marginBottom: '2%',
        overflow: 'hidden',
        borderRadius: 1,
        backgroundColor: '#546E7A',
        alignSelf: 'center',
    },
    submitEditProfileContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        // position: 'absolute',
        // bottom: 0,
        // backgroundColor:'yellow',
        // width: '60%',
        alignSelf: 'center'
    }

});