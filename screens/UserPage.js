import React, {Component} from 'react';
import {StyleSheet, TextInput, ScrollView, View, Text, Alert} from 'react-native';
import Courses from './Courses';
import Profile from './Profile';
import Notifications from './Notifications.js';
import * as firebase from "firebase";
import { Keyboard } from 'react-native'
import autobind from 'autobind-decorator';


import Tabbar from 'react-native-tabbar-bottom'

const config = {
    apiKey: "AIzaSyDLExTC3v_NByNGRMO310k4tKjoMPR5ekw",
    authDomain: "studentcatalog-cebe5.firebaseapp.com",
    databaseURL: "https://studentcatalog-cebe5.firebaseio.com",
    projectId: "studentcatalog-cebe5",
    storageBucket: "",
    messagingSenderId: "132643019625"
};

export default class UserPage extends Component {
    static navigationOptions = {
        header: null,
    }

    constructor(props) {
        super(props)
        this.state = {
            page: "CoursesScreen",
            isVisible: true,
            counter: 0,
            numberOfAllNotifications: 0,
            badgeLabel : 0
        }
        this.keyboardWillShow = this.keyboardWillShow.bind(this)
        this.keyboardWillHide = this.keyboardWillHide.bind(this)

    }
    componentDidMount(){
        this.getNumberOfAllNotifications();

    }

    componentWillMount() {
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        // let user = firebase.auth();
        // console.log(firebase.auth().currentUser);
        // Alert.alert(firebase.auth().currentUser.displayName);

        this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow)
        this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide)
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove()
        this.keyboardWillHideSub.remove()
    }

    keyboardWillShow = event => {
        this.setState({
            isVisible: false
        })
    }

    keyboardWillHide = event => {
        this.setState({
            isVisible: true
        })
    }

    @autobind
    getCounter(){
        // Alert.alert(this.state.numberOfAllNotifications.toString());
        var c=0;
        let username = firebase.auth().currentUser.email.split('@')[0];

        let ref = firebase.database().ref('log');
        ref.child("number"+username).on("value", (data) => {
            c = data.val();

            this.setState({counter: c}, this.getBadgeLabel)
            // Alert.alert("My courses:   " + returnArr.length);
        })

    }
    @autobind
    getNumberOfAllNotifications(){
        try {
            let user = firebase.auth().currentUser;
            var username = user.email.split('@')[0];
            let ref = firebase.database().ref('log');
            ref.orderByChild("date").on("value", (data) => {
                counter = 0;
                data.forEach(function (childSnapshot) {
                    let item = childSnapshot.val();
                    item.key = childSnapshot.key;
                    try {
                        if (item.username.localeCompare(username) === 0) {
                            counter = counter + 1;
                        }
                    } catch (e) {

                    }
                });
                this.setState({numberOfAllNotifications: counter}, this.getCounter)
            })
        }catch (e) {
            
        }
    }

    @autobind
    getBadgeLabel(){
        this.setState({badgeLabel: this.state.numberOfAllNotifications - this.state.counter});
    }
    render() {
        return (
            <View style={styles.container}>
                {
                    // if you are using react-navigation just pass the navigation object in your components like this:
                    // {this.state.page === "HomeScreen" && <MyComp navigation={this.props.navigation}>Screen1</MyComp>}
                }
                {this.state.page === "ProfileScreen" && <Profile navigation={this.props.navigation}/>}
                {this.state.page === "CoursesScreen" && <Courses navigation={this.props.navigation}/>}
                {this.state.page === "NotificationScreen" && <Notifications navigation={this.props.navigation}/>}

                {this.state.isVisible ?
                <Tabbar style={styles.tabbar}
                        stateFunc={(tab) => {
                            this.setState({page: tab.page})
                            //this.props.navigation.setParams({tabTitle: tab.title})
                        }}
                        tabbarBgColor='#263238'
                        activePage={this.state.page}
                        labelSize = {15}
                        selectedLabelColor = 'white'
                        selectedIconColor = 'white'
                        iconColor = '#607D8B'
                        labelColor = '#607D8B'
                        tabs={[
                            {
                                page: "ProfileScreen",
                                icon: "person",
                                iconText: "Profil"
                            },
                            {
                                page: "CoursesScreen",
                                icon: "albums",
                                iconText: "Cursuri"
                            },
                            {
                                page: "NotificationScreen",
                                icon: "notifications",
                                badgeNumber: this.state.badgeLabel,
                                iconText: "Notificari"
                            },
                        ]}
                />:
                    null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#607D8B',
        flex: 1,

    },
    tabbar: {}
});