// import React, {Component} from 'react';
// import {StyleSheet, TextInput, ScrollView, View, Text, Picker, Alert} from 'react-native';
// import Icon from 'react-native-fa-icons';
// import MyButton from '../components/MyButton';
// import autobind from 'autobind-decorator';
// import * as firebase from "firebase";
//
// const config = {
//     apiKey: "AIzaSyDLExTC3v_NByNGRMO310k4tKjoMPR5ekw",
//     authDomain: "studentcatalog-cebe5.firebaseapp.com",
//     databaseURL: "https://studentcatalog-cebe5.firebaseio.com",
//     projectId: "studentcatalog-cebe5",
//     storageBucket: "",
//     messagingSenderId: "132643019625"
// };
//
// let u = '';
// export default class Register extends Component {
//     static navigationOptions = {
//         header: null,
//     }
//
//     constructor(props) {
//         super(props);
//         this.state = {
//             firstName: 'Geanina',
//             lastName: 'Hutanu',
//             email: 'hgir1678@scs.ubbcluj.ro',
//             year: '3',
//             group: '234',
//             password: '123456',
//             cPassword: '123456',
//         };
//     }
//
//     /* Setters */
//     @autobind
//     setFirstName(firstName) {
//         this.setState({firstName});
//     }
//
//     @autobind
//     setLastName(lastName) {
//         this.setState({lastName});
//     }
//
//     @autobind
//     setEmail(email) {
//         this.setState({email});
//     }
//
//     @autobind
//     setGroup(group) {
//         this.setState({group});
//     }
//
//     @autobind
//     setPassword(password) {
//         this.setState({password});
//     }
//
//     @autobind
//     setConfirmedPassword(cPassword) {
//         this.setState({cPassword});
//     }
//
//     /* Initialize method */
//     componentWillMount() {
//         if(!firebase.apps.length){
//             firebase.initializeApp(config);
//         }
//     }
//
//
//     async verifyEmail(email) {
//
//         let username = email.split("@");
//         let ref = firebase.database().ref('students');
//         ref.orderByChild("username").equalTo(username[0]).on('value', (data) => {
//             data.forEach(function (child) {
//                 let item = child.val();
//                 item.key = child.key;
//                 u = item.username;
//                 if (u === '') {
//                     return false;
//                 }
//                 else {
//                     return true;
//
//                 }
//             })
//         });
//     }
//
//     async verifyCNP(cnp) {
//
//         let username = email.split("@");
//         let ref = firebase.database().ref('students');
//         ref.orderByChild("username").equalTo(username[0]).on('value', (data) => {
//             data.forEach(function (child) {
//                 let item = child.val();
//                 item.key = child.key;
//                 u = item.CNP;
//                 if (u === '' || u.localeCompare(cnp) !== 0) {
//                     return false;
//                 }
//                 else {
//                     return true;
//
//                 }
//             })
//         });
//     }
//
//     async onPressRegister() {
//         const {navigate} = this.props.navigation;
//         try {
//             if (this.verifyEmail(this.state.email)) {
//
//                 firebase.auth()
//                     .createUserAndRetrieveDataWithEmailAndPassword(this.state.email, this.state.password)
//                     .then(async (response) => {
//                         var username = response.user.email.split('@')[0];
//                         firebase.database().ref('students').child(username).set({
//                             firstName: this.state.firstName,
//                             lastName: this.state.lastName,
//                             email: this.state.email,
//                             year: this.state.year,
//                             group: this.state.group
//                         });
//                         try {
//                             await firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password);
//                             var user = firebase.auth().currentUser;
//
//                             user.updateProfile({
//                                 displayName: this.state.firstName + " " + this.state.lastName,
//                             }).then(function() {
//                                 navigate('UserPage');
//                             }).catch(function(error) {
//                                 // An error happened.
//                             });
//
//                         } catch (e) {
//                             console.log("Error at login from register.");
//                         }
//                     }, error => {
//                         console.log('Error from set up user info.', error.message);
//                     })
//             }
//         } catch (e) {
//             console.log("Error from onRegisterPress");
//         }
//     }
//
//     render() {
//         const {navigate} = this.props.navigation;
//         return (
//
//             <ScrollView contentContainerStyle={styles.contentScroll}>
//                 <View style={styles.inputView}>
//                     <Icon name='user' style={styles.iconStyle}/>
//                     <TextInput
//                         autoCorrect={false}
//                         underlineColorAndroid='transparent'
//                         style={{flex: 1, color: '#0047b3'}}
//                         placeholder='First name'
//                         // onChangeText={(firstName) => this.setState({firstName})}
//                         onChangeText={this.setFirstName}
//                         onSubmitEditing={() => this.lastNameRef.focus()}
//                     />
//                 </View>
//                 <View style={styles.inputView}>
//                     <Icon name='user' style={styles.iconStyle}/>
//                     <TextInput
//                         autoCorrect={false}
//                         underlineColorAndroid='transparent'
//                         style={{flex: 1, color: '#0047b3'}}
//                         placeholder='Last name'
//                         // onChangeText={(lastName) => this.setState({lastName})}
//                         onChangeText={this.setLastName}
//                         ref={lastNameRef => this.lastNameRef = lastNameRef}
//                         onSubmitEditing={() => this.emailRef.focus()}
//                     />
//                 </View>
//                 <View style={styles.inputView}>
//                     <Icon name='envelope-open' style={styles.iconStyle}/>
//                     <TextInput
//                         autoCorrect={false}
//                         underlineColorAndroid='transparent'
//                         style={{flex: 1, color: '#0047b3'}}
//                         placeholder='Email'
//                         // onChangeText={(email) => this.setState({email})}
//                         onChangeText={this.setEmail}
//                         ref={emailRef => this.emailRef = emailRef}
//                     />
//                 </View>
//                 <View style={styles.inputView}>
//                     <View style={{flexDirection: 'row', alignItems: 'center'}}>
//                         <View style={{flex: .7}}>
//                             <View style={styles.yearOfStudyStyle}>
//                                 <Icon name='university' style={styles.iconStyle}/>
//                                 <Text
//                                 >Year of study</Text>
//                             </View>
//                         </View>
//                         <View style={{flex: .3}}>
//                             <Picker
//                                 selectedValue={this.state.year}
//                                 style={{height: 50, width: 100}}
//                                 onValueChange={(itemValue, itemIndex) => this.setState({year: itemValue})}>
//                                 <Picker.Item label="1" value="1"/>
//                                 <Picker.Item label="2" value="2"/>
//                                 <Picker.Item label="3" value="3"/>
//                             </Picker>
//                         </View>
//                     </View>
//                 </View>
//                 <View style={styles.inputView}>
//                     <Icon name='users' style={styles.iconStyle}/>
//                     <TextInput
//                         autoCorrect={false}
//                         underlineColorAndroid='transparent'
//                         style={{flex: 1, color: '#0047b3'}}
//                         placeholder='Group'
//                         // onChangeText={(cPassword) => this.setState({cPassword})}
//                         onChangeText={this.setGroup}
//                         ref={groupRef => this.groupRef = groupRef}
//                         onSubmitEditing={() => this.passwordRef.focus()}
//
//                     />
//                 </View>
//                 <View style={styles.inputView}>
//                     <Icon name='unlock-alt' style={styles.iconStyle}/>
//                     <TextInput
//                         autoCorrect={false}
//                         underlineColorAndroid='transparent'
//                         style={{flex: 1, color: '#0047b3'}}
//                         placeholder='Password'
//                         secureTextEntry={true}
//                         // onChangeText={(password) => this.setState({password})}
//                         onChangeText={this.setPassword}
//                         ref={passwordRef => this.passwordRef = passwordRef}
//                         onSubmitEditing={() => this.cPasswordRef.focus()}
//                     />
//                 </View>
//                 <View style={styles.inputView}>
//                     <Icon name='unlock-alt' style={styles.iconStyle}/>
//                     <TextInput
//                         autoCorrect={false}
//                         underlineColorAndroid='transparent'
//                         style={{flex: 1, color: '#0047b3'}}
//                         placeholder='Retype password'
//                         secureTextEntry={true}
//                         // onChangeText={(cPassword) => this.setState({cPassword})}
//                         onChangeText={this.setConfirmedPassword}
//                         ref={cPasswordRef => this.cPasswordRef = cPasswordRef}
//                     />
//                 </View>
//                 <MyButton
//                     text='Submit'
//                     onPress={() => this.onPressRegister()}
//                 />
//             </ScrollView>
//         );
//     }
// }
//
// const
//     styles = StyleSheet.create({
//         contentScroll: {
//             //backgroundColor: '#f9e6ff',
//             backgroundColor: '#607D8B',
//             padding: 20,
//             justifyContent: 'center',
//             flexDirection: 'column',
//             flexGrow: 1
//         },
//         inputView: {
//             flexDirection: 'row',
//             justifyContent: 'center',
//             alignItems: 'center',
//             //backgroundColor: '#f2ccff',
//             backgroundColor: '#90A4AE',
//             height: 70,
//             // borderRadius: 20,
//             margin: 10
//         },
//         iconStyle: {
//             fontSize: 25,
//             color: '#263238',
//             padding: 10
//         },
//         container: {
//             padding: 10,
//         },
//         yearOfStudyStyle: {
//             flexDirection: 'row',
//             alignItems: 'center',
//         }
//     });
