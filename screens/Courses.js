import * as React from 'react';
import {View, StyleSheet, TouchableOpacity, Text, Alert, ScrollView, TextInput, Picker} from 'react-native';
import Icon from 'react-native-fa-icons';
import autobind from 'autobind-decorator';
import * as firebase from "firebase";
import MyRow from '../components/MyRow';
import {Dropdown} from 'react-native-material-dropdown';
import renderIf from '../components/renderIf';

const config = {
    apiKey: "AIzaSyDLExTC3v_NByNGRMO310k4tKjoMPR5ekw",
    authDomain: "studentcatalog-cebe5.firebaseapp.com",
    databaseURL: "https://studentcatalog-cebe5.firebaseio.com",
    projectId: "studentcatalog-cebe5",
    storageBucket: "",
    messagingSenderId: "132643019625"
};

export default class Courses extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayMyCourses: true,
            selectedTab: 1,
            courses: [],
            filteredCourses: [],
            displayFilteredCourses: false,
            myCourses: [],
            searchInput: '',
            searchSelected: false,
            searchedCourses: [],
            filterSelected: false,
            filterSubmitted: false,
            semester: 1
        }
    }

    @autobind
    setCourses() {
        this.setState({displayMyCourses: true});
        this.setState({selectedTab: 1});
        this.getCourses();
        this.getMyCourses();
    }

    @autobind
    setAdd() {
        this.setState({displayMyCourses: false});
        this.setState({selectedTab: 2});
        this.getCourses();
        this.getMyCourses();
    }

    @autobind
    setDelete() {
        this.setState({displayMyCourses: false});
        this.setState({selectedTab: 3});
        this.getCourses();
        this.getMyCourses();
    }

    @autobind
    async getCourses() {
        let ref = firebase.database().ref('courses');
        ref.orderByChild("name").startAt(0).on("value", (data) => {
            returnArr = [];

            data.forEach(function (childSnapshot) {
                let item = childSnapshot.val();
                item.key = childSnapshot.key;
                returnArr.push(item);
            });
            this.setState({courses: returnArr})
        })
    }

    @autobind
    async getMyCourses() {
        let user = firebase.auth().currentUser;
        let ref = firebase.database().ref('enrolledStudents');
        ref.orderByChild("studentEmail").equalTo(user.email).on("value", (data) => {
            returnArr = [];

            data.forEach(function (childSnapshot) {
                let item = childSnapshot.val();
                item.key = childSnapshot.key;
                returnArr.push(item);
            });
            this.setState({myCourses: returnArr})
            // Alert.alert("My courses:   " + returnArr.length);
        })
    }

    @autobind
    async filterCourses() {
        // this.setRefresh();
        let ref = firebase.database().ref('courses');
        ref.orderByChild("semester").equalTo(parseInt(this.state.semester)).on("value", (data) => {
            returnArr = [];

            data.forEach((childSnapshot) => {
                let item = childSnapshot.val();
                item.key = childSnapshot.key;
                if (!this.checkEnrolledCourse(item)) {
                    // Alert.alert("fferfe");
                    returnArr.push(item);
                }
            });

            // Alert.alert(returnArr.length.toString());
            this.setState({filteredCourses: []});
            this.setState({filteredCourses: returnArr});
        })
    }

    @autobind
    setFilter() {
        this.setRefresh();
        this.setState({filterSelected: !this.state.filterSelected});
        // this.setState({filteredCourses: []});
    }

    @autobind
    setFilterSubmitted(){
        this.setState({filterSubmitted: true});
        this.setState({filteredCourses: []}, this.filterCourses);
    }


    @autobind
    setSearch() {
        this.setState({searchSelected: true});
        this.setState({searchedCourses: []}, this.searchCourses);

    }
    @autobind
    setRefresh(){
        this.setState({searchSelected: false});
        this.setState({filterSelected: false});
        this.setState({filterSubmitted: false});

    }

    @autobind
    searchCourses() {
        returnArr = [];
        for (var i = 0; i < this.state.courses.length; i++) {
            if (!this.checkEnrolledCourse(this.state.courses[i])) {
                // Alert.alert(this.state.searchInput);
                if (this.state.courses[i].name.toLowerCase().indexOf(this.state.searchInput.toLowerCase()) !== -1) {
                    // Alert.alert(this.state.courses[i].name);
                    returnArr.push(this.state.courses[i]);
                }
            }
        }
// Alert.alert(searchedCourses.length.toString());
        this.setState({searchedCourses: []});
        this.setState({searchedCourses: returnArr});
        this.setState({searchInput:''});
// this.setState({searchSelected:false})
    }

    @autobind
    checkEnrolledCourse(course) {
        for (var i = 0; i < this.state.myCourses.length; i++) {
            if (this.state.myCourses[i].courseCode === course.key) {
                return true;
            }
        }
        return false;
    }

    componentWillMount() {
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        this.getCourses();
        this.getMyCourses();

    }

    render() {
        let dataDropDown = [{
            value: '1',
        }, {
            value: '2',
        }, {
            value: '3',
        }];
        return (
            <ScrollView style={styles.mainContainer}>
                <View style={styles.topMenuContainer}>
                    <View style={styles.leftContainer}>
                        <TouchableOpacity
                            onPress={this.setCourses}>
                            <Text style={[styles.leftTextStyle, this.state.selectedTab === 1 && {color: 'white'}]}>
                                Cursurile mele
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.middleContainer}>
                        <TouchableOpacity
                            onPress={this.setAdd}>
                            <Icon name='plus'
                                  style={[styles.iconStyle, this.state.selectedTab === 2 && {color: 'white'}]}/>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.rightContainer}>
                        <TouchableOpacity
                            onPress={this.setDelete}>
                            <Icon name='trash'
                                  style={[styles.iconStyle, this.state.selectedTab === 3 && {color: 'white'}]}/>
                        </TouchableOpacity>
                    </View>
                </View>


                <View style={styles.bottomContainer}>
                    {/*{this.state.displayMyCourses ?*/}
                    {renderIf(this.state.selectedTab === 1,
                        <View style={styles.scrollViewStyle}>
                            <ScrollView contentContainerStyle={styles.scrollContainer}>
                                <View>
                                    {this.state.courses.map((element, i) => (
                                        <View>
                                            {renderIf(this.checkEnrolledCourse(element),
                                                <MyRow
                                                    code={element.key}
                                                    name={element.name}
                                                    year={element.year}
                                                    package={element.package}
                                                    semester={element.semester}
                                                    credits={element.credits}
                                                    type={element.type}
                                                    professor={element.profesor}
                                                    navigation={this.props.navigation}
                                                    // isAdded = {false}
                                                    symbol='angle-right'
                                                >
                                                </MyRow>)}
                                        </View>
                                    ))}
                                </View>
                            </ScrollView>
                        </View>
                        // :
                    )}
                    {renderIf(this.state.selectedTab === 2,
                        <View style={styles.scrollViewStyle}>
                            <ScrollView contentContainerStyle={styles.scrollContainer}>
                                <View style={styles.searchAndFilterContainer}>
                                    <TouchableOpacity
                                        onPress={this.setFilter}
                                    >
                                        {!this.state.filterSelected ?
                                            <View>
                                                <Icon name='filter' style={{
                                                    fontSize: 30,
                                                    color: '#263238',
                                                    paddingTop: 2,
                                                    paddingBottom: 2,
                                                    paddingLeft: 10,
                                                    paddingRight: 10
                                                }}/>
                                            </View>
                                            :
                                            <View>
                                                <Icon name='times' style={{
                                                    fontSize: 30,
                                                    color: '#263238',
                                                    paddingTop: 2,
                                                    paddingBottom: 2,
                                                    paddingLeft: 10,
                                                    paddingRight: 10
                                                }}/>
                                            </View>
                                        }
                                    </TouchableOpacity>
                                    <View style={styles.textInputContainer}>
                                        <TextInput
                                            underlineColorAndroid='#263238'
                                            selectionColor='#263238'
                                            onChangeText={(searchInput) => this.setState({searchInput})}
                                            value={this.state.searchInput}
                                        />
                                    </View>

                                    <TouchableOpacity
                                        onPress={this.setSearch}
                                    >
                                        <View>
                                            <Icon name='search' style={{
                                                fontSize: 30,
                                                color: '#263238',
                                                paddingTop: 2,
                                                paddingBottom: 2,
                                                paddingLeft: 10,
                                                paddingRight: 10
                                            }}/>
                                        </View>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={this.setRefresh}
                                    >
                                        <View>
                                            <Icon name='refresh' style={{
                                                fontSize: 30,
                                                color: '#263238',
                                                paddingTop: 2,
                                                paddingBottom: 2,
                                                paddingLeft: 10,
                                                paddingRight: 10
                                            }}/>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                {renderIf((!this.state.searchSelected && !this.state.filterSelected),
                                    <View>
                                        {this.state.courses.map((element, i) => (
                                            <View>
                                                {renderIf(!this.checkEnrolledCourse(element),
                                                    // this.state.isEnrolled ?
                                                    <MyRow
                                                        code={element.key}
                                                        name={element.name}
                                                        year={element.year}
                                                        package={element.package}
                                                        semester={element.semester}
                                                        credits={element.credits}
                                                        type={element.type}
                                                        professor={element.profesor}
                                                        navigation={this.props.navigation}
                                                        // isAdded = {false}
                                                        symbol='plus'
                                                    >
                                                    </MyRow>)
                                                }

                                            </View>
                                        ))}
                                    </View>
                                )}
                                {renderIf(this.state.searchSelected,
                                    <View>
                                        {this.state.searchedCourses.map((element, i) => (
                                            <View>
                                                {renderIf(!this.checkEnrolledCourse(element),
                                                    // this.state.isEnrolled ?
                                                    <MyRow
                                                        code={element.key}
                                                        name={element.name}
                                                        year={element.year}
                                                        package={element.package}
                                                        semester={element.semester}
                                                        credits={element.credits}
                                                        type={element.type}
                                                        professor={element.profesor}
                                                        navigation={this.props.navigation}
                                                        // isAdded = {false}
                                                        symbol='plus'
                                                    >
                                                    </MyRow>)
                                                }

                                            </View>
                                        ))}
                                    </View>)}
                                {/*:*/}
                                {/*<View>*/}
                                {/*{this.state.searchedCourses.map((element, i) => (*/}
                                {/*<View>*/}
                                {/*{renderIf(!this.checkEnrolledCourse(element),*/}
                                {/*// this.state.isEnrolled ?*/}
                                {/*<MyRow*/}
                                {/*code={element.key}*/}
                                {/*name={element.name}*/}
                                {/*year={element.year}*/}
                                {/*package={element.package}*/}
                                {/*semester={element.semester}*/}
                                {/*credits={element.credits}*/}
                                {/*type={element.type}*/}
                                {/*professor={element.profesor}*/}
                                {/*navigation={this.props.navigation}*/}
                                {/*// isAdded = {false}*/}
                                {/*symbol='plus'*/}
                                {/*>*/}
                                {/*</MyRow>)*/}
                                {/*}*/}

                                {/*</View>*/}
                                {/*))}*/}
                                {/*</View>*/}
                                {/*}*/}


                                {renderIf(this.state.filterSelected && !this.state.filterSubmitted,
                                    <View style={{marginLeft: '5%', marginRight: '5%'}}>
                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}>
                                            <View>
                                                {/*<View style={{flexDirection:'row'}}>*/}
                                                {/*<Icon name='university'/>*/}
                                                <Text style={{
                                                    textAlign: 'center',
                                                    fontSize: 17,
                                                    fontWeight: 'bold'
                                                }}>Semestru:</Text>
                                                {/*</View>*/}
                                            </View>
                                            <View style={{flex: .5}}>
                                                <Picker
                                                    selectedValue={this.state.semester}
                                                    // prompt = 'Semestru'
                                                    // style={{height: 50, width: 100}}
                                                    mode = 'dropdown'
                                                    onValueChange={(itemValue) => this.setState({semester:itemValue})}>
                                                    <Picker.Item label="1" value="1"/>
                                                    <Picker.Item label="2" value="2"/>
                                                    <Picker.Item label="3" value="3"/>
                                                    <Picker.Item label="4" value="4"/>
                                                    <Picker.Item label="5" value="5"/>
                                                    <Picker.Item label="6" value="6"/>
                                                </Picker>
                                            </View>
                                            <View>
                                                <TouchableOpacity
                                                    style={{backgroundColor: '#263238', padding: '5%'}}
                                                    onPress={this.setFilterSubmitted}
                                                >
                                                    <Text style={{
                                                        textAlign: 'center',
                                                        fontSize: 17,
                                                        fontWeight: 'bold',
                                                        color: 'white'
                                                    }}>Filtreaza</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        {this.state.courses.map((element, i) => (
                                            <View>
                                                {renderIf(!this.checkEnrolledCourse(element),
                                                    // this.state.isEnrolled ?
                                                    <MyRow
                                                        code={element.key}
                                                        name={element.name}
                                                        year={element.year}
                                                        package={element.package}
                                                        semester={element.semester}
                                                        credits={element.credits}
                                                        type={element.type}
                                                        professor={element.profesor}
                                                        navigation={this.props.navigation}
                                                        // isAdded = {false}
                                                        symbol='plus'
                                                    >
                                                    </MyRow>)
                                                }
                                            </View>
                                        ))}
                                    </View>)}

                                {renderIf(this.state.filterSelected && this.state.filterSubmitted,
                                    <View style={{marginLeft: '5%', marginRight: '5%'}}>
                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}>
                                            <View>
                                                {/*<View style={{flexDirection:'row'}}>*/}
                                                {/*<Icon name='university'/>*/}
                                                <Text style={{
                                                    textAlign: 'center',
                                                    fontSize: 17,
                                                    fontWeight: 'bold'
                                                }}>Semestru:</Text>
                                                {/*</View>*/}
                                            </View>
                                            <View style={{flex: .5}}>
                                                <Picker
                                                    selectedValue={this.state.semester}
                                                    mode = 'dropdown'
                                                    // prompt = 'Semestru'
                                                    // style={{height: 50, width: 100}}
                                                    // onValueChange={(semester) => this.setState(semester)}>
                                                    onValueChange={(itemValue) => this.setState({semester: itemValue})}>
                                                    <Picker.Item label="1" value="1"/>
                                                    <Picker.Item label="2" value="2"/>
                                                    <Picker.Item label="3" value="3"/>
                                                    <Picker.Item label="4" value="4"/>
                                                    <Picker.Item label="5" value="5"/>
                                                    <Picker.Item label="6" value="6"/>
                                                </Picker>
                                            </View>
                                            <View>
                                                <TouchableOpacity
                                                    style={{backgroundColor: '#263238', padding: '5%'}}
                                                    onPress={this.setFilterSubmitted}
                                                >
                                                    <Text style={{
                                                        textAlign: 'center',
                                                        fontSize: 17,
                                                        fontWeight: 'bold',
                                                        color: 'white'
                                                    }}>Filtreaza</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        {this.state.filteredCourses.map((element, i) => (
                                            <View>
                                                {renderIf(!this.checkEnrolledCourse(element),
                                                    // this.state.isEnrolled ?
                                                    <MyRow
                                                        code={element.key}
                                                        name={element.name}
                                                        year={element.year}
                                                        package={element.package}
                                                        semester={element.semester}
                                                        credits={element.credits}
                                                        type={element.type}
                                                        professor={element.profesor}
                                                        navigation={this.props.navigation}
                                                        // isAdded = {false}
                                                        symbol='plus'
                                                    >
                                                    </MyRow>)
                                                }
                                            </View>
                                        ))}
                                    </View>)}


                                {/*:*/}
                                {/*<View>*/}
                                {/*{this.state.courses.map((element, i) => (*/}
                                {/*<View>*/}
                                {/*{renderIf(!this.checkEnrolledCourse(element),*/}
                                {/*// this.state.isEnrolled ?*/}
                                {/*<MyRow*/}
                                {/*code={element.key}*/}
                                {/*name={element.name}*/}
                                {/*year={element.year}*/}
                                {/*package={element.package}*/}
                                {/*semester={element.semester}*/}
                                {/*credits={element.credits}*/}
                                {/*type={element.type}*/}
                                {/*professor={element.profesor}*/}
                                {/*navigation={this.props.navigation}*/}
                                {/*// isAdded = {false}*/}
                                {/*symbol='plus'*/}
                                {/*>*/}
                                {/*</MyRow>)*/}
                                {/*}*/}

                                {/*</View>*/}
                                {/*))}*/}
                                {/*</View>*/}
                                {/*}*/}
                            </ScrollView>
                        </View>
                    )}

                    {renderIf(this.state.selectedTab === 3,
                        <View style={styles.scrollViewStyle}>
                            {/*<View style={styles.dropDownStyle}>*/}
                            {/*<Dropdown*/}
                            {/*label='An de studiu'*/}
                            {/*labelFontSize='15'*/}
                            {/*data={dataDropDown}*/}
                            {/*selectedItemColor='#263238'*/}
                            {/*pickerStyle={{backgroundColor: '#607D8B', alignItems: 'center'}}*/}
                            {/*onChangeText={this.getCoursesByYear.bind(this)}*/}
                            {/*/>*/}
                            {/*</View>*/}
                            <ScrollView contentContainerStyle={styles.scrollContainer}>
                                <View>
                                    {this.state.courses.map((element, i) => (
                                        <View>
                                            {renderIf(this.checkEnrolledCourse(element),
                                                <MyRow
                                                    code={element.key}
                                                    name={element.name}
                                                    year={element.year}
                                                    package={element.package}
                                                    semester={element.semester}
                                                    credits={element.credits}
                                                    type={element.type}
                                                    professor={element.profesor}
                                                    navigation={this.props.navigation}
                                                    // isAdded = {false}
                                                    symbol='trash'
                                                >
                                                </MyRow>)}
                                            {/*}*/}

                                        </View>
                                    ))}
                                </View>
                            </ScrollView>
                        </View>
                    )}

                    {/*}*/}
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginBottom: '1%'
    },
    topMenuContainer: {
        flex: .07,
        flexDirection: "row",
        backgroundColor: '#263238',
        marginLeft: '1%',
        marginRight: '1%',
        marginTop: '1%',
        padding:3
    },
    bottomContainer: {
        flex: .93,
        // backgroundColor:'red'
    },
    leftContainer: {
        flex: .76,
        // paddingLeft: 20,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor:'red'
    },
    middleContainer: {
        flex: .13,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor:'yellow'
    },
    rightContainer: {
        flex: .13,
        alignItems: 'center',
        justifyContent: 'center',
        // marginRight:20,
        // backgroundColor:'blue'
    },
    iconStyle: {
        fontSize: 25,
        color: '#607D8B',
        padding: '1%'
    },
    leftTextStyle: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#607D8B',
    },
    scrollContainer: {
        justifyContent: 'center',
        // backgroundColor:'red',
        paddingBottom: '8%',
        paddingTop: '1%'
        // marginTop: 1,
    },
    scrollViewStyle: {
        // marginBottom: '8%',
        // marginTop: 1,
        justifyContent: 'center',
        paddingTop: '1%',
        paddingBottom: '1%'
    },
    dropDownStyle: {
        marginLeft: '30%',
        marginRight: '30%'
    },
    searchAndFilterContainer: {
        flexDirection: 'row',
        marginLeft: '1%',
        marginRight: '1%',
        // // backgroundColor:'red',
        // // alignSelf:'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInputContainer: {
        // borderColor:'black',
        // borderWidth:1,
        flex: .8
    }
});