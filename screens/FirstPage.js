import React from 'react';
import { StyleSheet, View, ScrollView, Image, Text, Button, TouchableOpacity, TextInput } from 'react-native';
import MyButton from '../components/MyButton.js';
import Icon from 'react-native-fa-icons';
import * as firebase from "firebase";
import autobind from 'autobind-decorator';


const config = {
    apiKey: "AIzaSyDLExTC3v_NByNGRMO310k4tKjoMPR5ekw",
    authDomain: "studentcatalog-cebe5.firebaseapp.com",
    databaseURL: "https://studentcatalog-cebe5.firebaseio.com",
    projectId: "studentcatalog-cebe5",
    storageBucket: "",
    messagingSenderId: "132643019625"
};

export default class FirstPage extends React.Component {

    static navigationOptions = {
        header: null,
    }
    /* States */
    // state = {
    //     email: '',
    //     password: '',
    // }

    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            registered: false,
            firstName:'',
            lastName:''
        }
    }

    /* Setters */
    @autobind
    setEmail(email) {
        this.setState({ email });
    }

    @autobind
    setPassword(password) {
        this.setState({ password });
    }

    /* Initialize */
    componentWillMount() {
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
    }

    async verifyEmail(email) {

        let username = email.split("@");
        let ref = firebase.database().ref('students');
        ref.orderByChild("username").equalTo(username[0]).on('value', (data) => {
            data.forEach(function (child) {
                let item = child.val();
                item.key = child.key;
                u = item.username;
                if (u === '') {
                    return false;
                }
                else {
                    return true;

                }
            })
        });
    }

    async verifyCNP(cnp) {

        let username = email.split("@");
        let ref = firebase.database().ref('students');
        ref.orderByChild("username").equalTo(username[0]).on('value', (data) => {
            data.forEach(function (child) {
                let item = child.val();
                item.key = child.key;
                u = item.CNP;
                if (u === '' || u.localeCompare(cnp) !== 0) {
                    return false;
                }
                else {
                    return true;

                }
            })
        });
    }

    async loginOrRegister(){
        // alert("Ajunge aici!");

        if(this.state.registered){
            firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
                .catch((e) => {alert(e)});
        }
        else {
            // alert("Ajunge aici!");
            if (this.verifyEmail(this.state.email) && this.verifyCNP(this.state.password)) {
                alert("Ajunge aici!");
                firebase.auth()
                    .createUserAndRetrieveDataWithEmailAndPassword(this.state.email, this.state.password)
                    .then(async (response) => {
                        var username = response.user.email.split('@')[0];
                        firebase.database().ref('students').child(username).update({
                            registered:true
                        });
                        this.setState({registered:true});
                        try {
                            // const { navigate } = this.props.navigation;
                            await firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
                                .then(()=>{
                                    var user = firebase.auth().currentUser;

                                    user.updateProfile({
                                        displayName: this.state.firstName + " " + this.state.lastName,
                                    });
                                }).catch((e)=> {
                                    alert(e);
                                });

                        } catch (e) {
                            console.log("Error at login.");
                        }
                    }, error => {
                        console.log('Error from set up user info.', error.message);
                    })
            }
            else {
                alert("Nu coincide email-ul cu parola")
            }
        }
    }

    async onPressLogin() {
        if(this.state.email === '' || this.state.password ===''){
            alert("All fields are mandatory!");
        }
        else {
            try {
                let username = this.state.email.split("@");
                let ref = firebase.database().ref('students/' + username[0] + '/');
                // alert(username[0]);
                ref.on('value', (data) => {
                    if (data.exists()) {
                        this.setState({firstName: data.val().firstName});
                        this.setState({lastName: data.val().lastName});
                        // alert(data.val().registered);
                        if (data.val().registered) {
                            this.setState({registered: true}, this.loginOrRegister);
                        }
                        else {
                            this.loginOrRegister();
                        }
                    }
                    else {
                        alert("Invalid data!");
                    }
                })
            } catch (e) {
                console.log("Error from onLoginPress");
            }
        }
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView contentContainerStyle={styles.contentScroll}>
                <View style={styles.imageView} >
                    <Image source={require('./resources/logo4.png')} style={styles.imageStyle} />
                </View>
                <View style={styles.inputView}>
                    {/* <Image source={require('../images/emailicon5.png')} style={styles.ImageStyle} /> */}
                    <Icon name='envelope-open' style={styles.iconStyle} />
                    <TextInput
                        autoCorrect={false}
                        underlineColorAndroid='transparent'
                        style={{ flex: 1, color: '#0047b3' }}
                        placeholder="Email"
                        onChangeText={this.setEmail}
                        onSubmitEditing={() => this.passwordRef.focus()} />
                </View>
                <View style={styles.inputView}>
                    {/* <Image source={require('../images/password6.png')} style={styles.ImageStyle} /> */}
                    <Icon name='unlock-alt' style={styles.iconStyle} />
                    <TextInput
                        autoCorrect={false}
                        underlineColorAndroid='transparent'
                        style={{ flex: 1, color: '#0047b3' }}
                        placeholder="Password"
                        secureTextEntry={true}
                        onChangeText={this.setPassword}
                        ref={passwordRef => this.passwordRef = passwordRef} />
                </View>
                <MyButton
                    text='Sign in'
                    onPress={() => this.onPressLogin()}
                />
                {/*<View style={styles.linkRegister}>*/}
                    {/*<Text*/}
                        {/*style={{ color: '#0D47A1', fontSize: 15 }}*/}
                        {/*onPress={() => navigate('Register')}>*/}
                        {/*Don't have an account? Register here!*/}
                    {/*</Text>*/}
                {/*</View>*/}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    contentScroll: {
        backgroundColor: '#607D8B',
        padding: 10,
        justifyContent: 'center',
        flexDirection: 'column',
        //alignItems: 'center',
        flexGrow: 1
    },
    imageView: {
        paddingTop: 10,
        paddingBottom: 20
    },
    imageStyle: {
        //   padding: 5,
        margin: 5,
        height: 230,
        width: 200,
        resizeMode: 'stretch',
        alignSelf: "center",
        backgroundColor:'#607D8B'
    },
    welcomeText: {
        paddingBottom: 10,
        fontSize: 30,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    inputView: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#90A4AE',
        height: 70,
        // borderRadius: 20,
        margin: 10
    },
    iconStyle: {
        fontSize: 25,
        color: '#263238',
        padding: 10
    },
    containerButton: {
        fontSize: 40
    },
    linkRegister: {
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
});