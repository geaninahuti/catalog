import * as React from 'react';
import {View, StyleSheet, Text, Alert, ScrollView, TouchableOpacity} from 'react-native';
import autobind from 'autobind-decorator';
import * as firebase from "firebase";
import renderIf from '../components/renderIf';
import {Table, TableWrapper, Row, Rows, Col, Cols, Cell} from 'react-native-table-component';

const config = {
    apiKey: "AIzaSyDLExTC3v_NByNGRMO310k4tKjoMPR5ekw",
    authDomain: "studentcatalog-cebe5.firebaseapp.com",
    databaseURL: "https://studentcatalog-cebe5.firebaseio.com",
    projectId: "studentcatalog-cebe5",
    storageBucket: "",
    messagingSenderId: "132643019625"
};

export default class Attendances extends React.Component {
    static navigationOptions = {
        header: null,
    }
    state = {
        user: '',
        courseName: '',
        courseCode: '',
        tableHead: ['Numar', 'Data','Profesor'],
        widthArr: [1, 2, 3],
        tableLabData: [],
        tableSeminarData: [],
        tableCursData: [],
        msgError: false,
        selectedTab: 1,
        nrOfLAbAttendances: 0,
        nrOfSeminarAttendances: 0,
        nrOfCursAttendances: 0
    }

    constructor(props) {
        super(props);
    }

    @autobind
    componentDidMount() {
        this.setState({courseCode: this.props.navigation.getParam('courseCode', '0')});
        this.setState({courseName: this.props.navigation.getParam('courseName', 'noName')});
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        this.setState({user: firebase.auth().currentUser.displayName});
        this.getAllLabAttendances();
        this.getAllSeminarAttendances();
        this.getAllCursAttendances();
    }

    @autobind
    async getAllLabAttendances() {
        this.setState({msgError: true});
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];

        let ref = firebase.database().ref('attendances');

        var childKey = username + this.props.navigation.getParam('courseCode', '0');
        ref.child(childKey).on("value", (entry) => {
            returnArrLab = [];

            try {
                var labAttendances = entry.val().laborator;
                // Alert.alert(labAttendances.length.toString());
                for(var k in labAttendances){
                    var v1 = labAttendances[k].profesor;
                    var v2 = labAttendances[k].date;
                    var v3 = labAttendances[k].nr;

                    var labAttendance = [
                        v3,
                        v2,
                        v1
                    ];
                    returnArrLab.push(labAttendance);
                }
                // labAttendances.forEach(function (childSnapshot) {
                //     // Alert.alert("hei!");
                //     var v1 = childSnapshot.profesor;
                //     var v2 = childSnapshot.date;
                //     var v3 = childSnapshot.nr;
                //
                //     var labAttendance = [
                //         v3,
                //         v2,
                //         v1
                //     ];
                //     returnArrLab.push(labAttendance);
                // });
                this.setState({tableLabData: returnArrLab});
                this.setState({nrOfLAbAttendances:returnArrLab.length})
            } catch (e) {
                // Alert.alert("Nicio nota de afisat!")
            }
        })
        // Alert.alert(returnArrLab.length.toString());
    }

    @autobind
    async getAllSeminarAttendances() {
        this.setState({msgError: true});
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];

        let ref = firebase.database().ref('attendances');

        var childKey = username + this.props.navigation.getParam('courseCode', '0');

        ref.child(childKey).on("value", (entry) => {
            returnArrSeminar = [];

            try {
                var seminarAttendances = entry.val().seminar;
                // Alert.alert(labGrades.length.toString());
                for(var k in seminarAttendances){
                    var v1 = seminarAttendances[k].profesor;
                    var v2 = seminarAttendances[k].date;
                    var v3 = seminarAttendances[k].nr;

                    var seminarAttendance = [
                        v3,
                        v2,
                        v1
                    ];
                    returnArrSeminar.push(seminarAttendance);
                }

                this.setState({tableSeminarData: returnArrSeminar});
                this.setState({nrOfSeminarAttendances:returnArrSeminar.length})
            } catch (e) {
                // Alert.alert("Nicio nota de afisat!")
            }
        })
        // Alert.alert(returnArrLab.length.toString());
    }

    @autobind
    async getAllCursAttendances() {
        this.setState({msgError: true});
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];

        let ref = firebase.database().ref('attendances');

        var childKey = username + this.props.navigation.getParam('courseCode', '0');

        ref.child(childKey).on("value", (entry) => {
            returnArrCurs = [];

            try {
                var cursAttendances = entry.val().curs;
                // Alert.alert(labGrades.length.toString());
                for(var k in cursAttendances){
                    var v1 = cursAttendances[k].profesor;
                    var v2 = cursAttendances[k].date;
                    var v3 = cursAttendances[k].nr;

                    var cursAttendance = [
                        v3,
                        v2,
                        v1
                    ];
                    returnArrCurs.push(cursAttendance);
                }

                this.setState({tableCursData: returnArrCurs});
                this.setState({nrOfCursAttendances:returnArrCurs.length})
            } catch (e) {
                // Alert.alert("Nicio nota de afisat!")
            }
        })
        // Alert.alert(returnArrLab.length.toString());
    }

    @autobind
    setTab(nr) {
        this.setState({selectedTab: nr});
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.mainContainer}>
                <View style={styles.nameContainer}>
                    <Text style={styles.nameStyle}>{this.state.courseName}</Text>
                </View>

                <View style={styles.gradesContainer}>
                    <View style={[styles.TOContainer, (this.state.selectedTab === 1) && {borderColor: 'white'}]}>
                        <TouchableOpacity
                            onPress={() => this.setTab(1)}
                        >
                            <Text
                                style={[styles.TOText, (this.state.selectedTab === 1) && {color: 'white'}]}>Laborator
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.counterContainer}>
                            <Text style={styles.counterText}>{this.state.nrOfLAbAttendances}</Text>
                        </View>
                    </View>

                    <View style={[styles.TOContainer, (this.state.selectedTab === 2) && {borderColor: 'white'}]}>
                        <TouchableOpacity
                            onPress={() => this.setTab(2)}
                        >
                            <Text
                                style={[styles.TOText, (this.state.selectedTab === 2) && {color: 'white'}]}>Seminar
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.counterContainer}>
                            <Text style={styles.counterText}>{this.state.nrOfSeminarAttendances}</Text>
                        </View>
                    </View>

                    <View style={[styles.TOContainer, (this.state.selectedTab === 3) && {borderColor: 'white'}]}>
                        <TouchableOpacity
                            onPress={() => this.setTab(3)}
                        >
                            <Text
                                style={[styles.TOText, (this.state.selectedTab === 3) && {color: 'white'}]}>Curs
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.counterContainer}>
                            <Text style={styles.counterText}>{this.state.nrOfCursAttendances}</Text>
                        </View>
                    </View>
                </View>
                    <View style={styles.tableContainer}>
                        <Table borderStyle={{borderColor: '#C1C0B9', borderTopColor: 'black'}}>
                            <Row data={this.state.tableHead} flexArr={this.state.widthArr}
                                 style={styles.header} textStyle={styles.textHeader}/>
                        </Table>
                        <ScrollView style={styles.dataWrapper}>
                            {renderIf(this.state.selectedTab === 1,
                                <Table borderStyle={{borderColor: '#C1C0B9'}} >
                                    {
                                        this.state.tableLabData.map((rowData, index) => (
                                            <Row
                                                key={index}
                                                data={rowData}
                                                flexArr={this.state.widthArr}
                                                style={styles.row}
                                                textStyle={styles.text}
                                            />
                                        ))
                                    }
                                </Table>)}
                            {renderIf(this.state.selectedTab === 2,
                                <Table borderStyle={{borderColor: '#C1C0B9'}}>
                                    {
                                        this.state.tableSeminarData.map((rowData, index) => (
                                            <Row
                                                key={index}
                                                data={rowData}
                                                flexArr={this.state.widthArr}
                                                style={styles.row}
                                                textStyle={styles.text}
                                            />
                                        ))
                                    }
                                </Table>)}
                            {renderIf(this.state.selectedTab === 3,
                                <Table borderStyle={{borderColor: '#C1C0B9'}}>
                                    {
                                        this.state.tableCursData.map((rowData, index) => (
                                            <Row
                                                key={index}
                                                data={rowData}
                                                flexArr={this.state.widthArr}
                                                style={styles.row}
                                                textStyle={styles.text}
                                            />
                                        ))
                                    }
                                </Table>)}
                        </ScrollView>
                    </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        // flex: 0.93,
        flex:1,
        backgroundColor: '#607D8B',
        // backgroundColor:'blue'
    },
    nameStyle: {
        fontSize: 20,
        alignSelf: 'center',
        textAlign: 'center',
        color: 'white'
    },
    nameContainer: {
        marginTop: '2%',
        marginBottom: '2%',
        // justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor:'red'
        // flex:.0
    },
    logOutContainer: {
        width: '100%',
        height: '10%',
        justifyContent: 'center',
        alignItems: 'center',
        // position: 'absolute',
        // bottom: 0
        marginBottom:'10%'
    },
    gradesContainer: {
        margin: 5,
        // backgroundColor: '#263238',
        backgroundColor: '#607D8B',
        // backgroundColor:'red',
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'space-between',
        justifyContent: 'center',
        paddingLeft: '5%',
        paddingRight: '5%',
        // flex:.0
    },
    TOContainer: {
        // padding:2,
        // backgroundColor:'red',
        alignSelf:'center',
        borderColor:'#263238',
        // borderColor:'yellow',
        // borderWidth:0.5,
        borderBottomWidth:1,
        paddingLeft:2,
        paddingRight:2,
        marginLeft:'3%',
        marginRight:'3%',
        flexDirection:'row'
    },
    TOText: {
        fontSize: 20,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: 'bold',
        // lineHeight: 30,
        // color: '#607D8B',
        color: '#263238',

    },

    tableContainer: { flex:1, padding: '1%', paddingTop: '2%', backgroundColor: '#607D8B'},
    tableContainerCurs:{flex: 1, padding: '8%', paddingTop: '3%', backgroundColor: '#607D8B', paddingBottom: '2%'},
    header: {height: 50, backgroundColor: '#263238'},
    textHeader: {color: 'white', textAlign: 'center', fontWeight: 'bold', fontSize: 15},
    text: {textAlign: 'center', fontWeight: 'bold'},
    dataWrapper: {marginTop: -1, marginBottom:'10%'},
    row: {height: 50, backgroundColor: '#607D8B'},
    leaveCourseText: {
        fontSize: 15,
        fontWeight: 'bold',
        textDecorationLine: 'underline'
    },
    counterContainer:{
        borderColor:'white',
        borderWidth:1,
        height:24,
        width:24,
        borderRadius:12,
        backgroundColor:'white',
        marginLeft:5
    },
    counterText:{
        fontSize: 15,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: 'bold',
        // lineHeight: 30,
        // color: '#607D8B',
        color: '#263238',
    }
});