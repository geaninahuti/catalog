import React from 'react'
import {View, Text, ActivityIndicator, StyleSheet, ScrollView, Alert} from 'react-native'
import Firebase from '../components/Firebase';
import * as firebase from "firebase";
import renderIf from "../components/renderIf";
import autobind from 'autobind-decorator';


const config = {
    apiKey: "AIzaSyDLExTC3v_NByNGRMO310k4tKjoMPR5ekw",
    authDomain: "studentcatalog-cebe5.firebaseapp.com",
    databaseURL: "https://studentcatalog-cebe5.firebaseio.com",
    projectId: "studentcatalog-cebe5",
    storageBucket: "",
    messagingSenderId: "132643019625"
};

export default class Notifications extends React.Component {
    static navigationOptions = {
        header: null,
    }
    // componentWillMount(){
    //     Firebase.init();
    // }

    state = {
        notifications: [],
        counter: 0
    }

    componentDidMount() {
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        this.getNotifications();
    }

    @autobind
    async getNotifications() {
        var returnArr = [];
        try {
            let user = firebase.auth().currentUser;
            var username = user.email.split('@')[0];
            let ref = firebase.database().ref('log');
            ref.orderByChild("date").on("value", (data) => {
                returnArr = [];

                data.forEach(function (childSnapshot) {
                    let item = childSnapshot.val();
                    item.key = childSnapshot.key;
                    try {
                        if (item.username.localeCompare(username) === 0) {
                            returnArr.push(item);
                        }
                    } catch (e) {

                    }
                });
                returnArr.reverse();
                this.setState({notifications: returnArr});
                this.setState({counter: returnArr.length}, this.updateNumber);
            })
        }catch (e) {

        }
    }

    @autobind
    updateNumber(){
        let user = firebase.auth().currentUser;
        var username = user.email.split('@')[0];
        let nr="number"+username;
        firebase.database().ref('log/').update({
            [nr]: this.state.counter,
        });

    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <ScrollView contentContainerStyle={styles.scrollContainer}>
                    <View>
                        {this.state.notifications.map((element, i) => (
                            <View style={styles.notificationView}>
                                <Text style={styles.dateStyle}>
                                    {element.date}
                                </Text>
                                <Text style={styles.courseStyle}>
                                    {element.courseName}
                                </Text>
                                <Text style={styles.professorStyle}>
                                    {element.professor}
                                </Text>
                                {element.type.contains("nota") >0  ?
                                    <Text style={styles.gradeStyle}>
                                        {element.type}: {element.grade}
                                    </Text> :
                                    <Text style={styles.gradeStyle}>
                                        {element.type}
                                    </Text>
                                }
                            </View>
                        ))}
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 0.9,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10
    },
    scrollContainer: {
        justifyContent: 'center',
        // backgroundColor:'red',
        paddingBottom: '8%',
        paddingTop: '1%'
        // marginTop: 1,
    },
    notificationView: {
        backgroundColor: '#546E7A',
        padding: '5%',
        marginLeft: '5%',
        marginRight: '5%',
        marginBottom: '5%'
    },
    dateStyle: {
        textAlign: 'center',
        color: 'white',
        fontSize: 18
    },
    courseStyle: {
        textAlign: 'center',
        fontSize: 17
    },
    professorStyle: {
        textAlign: 'center',
        fontSize: 16
    },
    gradeStyle: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20
    }

})