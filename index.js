import { AppRegistry } from 'react-native';
import {StackNavigator} from 'react-navigation';
import FirstPage from './screens/FirstPage.js';
import UserPage from './screens/UserPage.js';
import Loading from './components/Loading.js';
import CatalogPage from './screens/CatalogPage.js';


console.disableYellowBox = true;

const App = StackNavigator({
    Loading: {screen : Loading},
    FirstPage: { screen: FirstPage },
    UserPage: { screen: UserPage },
    CatalogPage:{screen: CatalogPage}
  });

AppRegistry.registerComponent('studentcatalog', () => App);
