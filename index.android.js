import { AppRegistry } from 'react-native';
import {StackNavigator} from 'react-navigation';
import FirstPage from './screens/FirstPage.js';
// import Register from './screens/Register.js';
import UserPage from './screens/UserPage.js';
import Loading from './components/Loading.js';


console.disableYellowBox = true;
const App = StackNavigator({
    Loading: {screen : Loading},
    FirstPage: { screen: FirstPage },
    // Register: { screen: Register },
    UserPage: { screen: UserPage },
});
AppRegistry.registerComponent('studentcatalog', () => App);
