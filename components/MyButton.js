import React from 'react';
import { StyleSheet, View} from 'react-native';
import Button from 'react-native-button';

const myButton = props => {
    return (
    <View style={styles.container}>
        <Button
            containerStyle={styles.containerButton}
            disabledContainerStyle={{ backgroundColor: '#602060' }}
            style={{fontSize: 20, color: 'white' }}
            onPress={props.onPress}>
            {props.text}
        </Button>
    </View>
    )
}
const styles = StyleSheet.create({
    container: {
        paddingBottom: 10,
        paddingTop: 10,
        marginLeft: 70,
        marginRight : 70,
    },
    containerButton: {
        padding: 15,
        height: 60, 
        overflow: 'hidden',
        backgroundColor: '#263238',
    },
});

export default myButton