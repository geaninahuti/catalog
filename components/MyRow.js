import React from 'react';
import {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, Text, Alert} from 'react-native';
import Icon from 'react-native-fa-icons';
import * as firebase from "firebase";
import autobind from 'autobind-decorator';
import renderIf from "./renderIf";



export default class MyRow extends React.Component {

    state = {
        showMoreDetails: false,
        showAllDetails: false,
        name: '',
        credits: '',
        semester: '',
        type: '',
        package: '',
        year: '',
        code: '',
        symbol: '',
        grades: [],
    };

    constructor(props) {
        super(props);

        this.state = {
            code: this.props.code,
            name: this.props.name,
            credits: this.props.credits,
            semester: this.props.semester,
            type: this.props.type,
            package: this.props.package,
            professor: this.props.professor,
            year: this.props.year,
            symbol: this.props.symbol,
        };
    }

    changeMoreDetails = () => {
        this.setState({showMoreDetails: !this.state.showMoreDetails});
    };

    @autobind
    async addCourse(){
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];
        console.log("Code" + this.state.code);
        let ref = firebase.database().ref('enrolledStudents');
        var childKey = username+this.state.code;

        ref.child(childKey).set({
            studentName: currentUser.displayName,
            studentEmail: currentUser.email,
            courseName: this.state.name,
            courseCode: this.state.code,
        });

    }

    @autobind
    async deleteCourse(){
        var hasGrades = false;
        const {navigate} = this.props.navigation;
        var currentUser = firebase.auth().currentUser;
        var username = currentUser.email.split('@')[0];
        let ref = firebase.database().ref('grades');
        var childKey = username + this.state.code;
        ref.child(childKey).on("value", (entry) => {
            try {
                var labGrades = entry.val().laborator;
                if (labGrades.length > 0) {
                    hasGrades = true;
                }
            } catch (e) {
                // Alert.alert("Nu exista note la materia asta!")
            }
        });
        if (hasGrades) {
            Alert.alert(
                'Esti sigur ca vrei sa parasesti cursul?',
                'Exista note la aceasta materie.',
                [
                    {text: 'Cancel', style: 'cancel'},
                    {
                        text: 'OK', onPress: () => {
                            firebase.database().ref('enrolledStudents').child(childKey).remove();
                            // navigate('UserPage')
                        }
                    },
                ],
            )
        }
        else {
            Alert.alert(
                'Esti sigur ca vrei sa parasesti cursul?',
                ' ',
                [
                    {text: 'Cancel', style: 'cancel'},
                    {
                        text: 'OK', onPress: () => {
                            firebase.database().ref('enrolledStudents').child(childKey).remove();
                            // navigate('UserPage')
                        }
                    },
                ],
            )
        }
    }

    @autobind
    async showGrades(){
        const {navigate} = this.props.navigation;
        navigate('CatalogPage', {courseCode:this.state.code, courseName:this.state.name});
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.containerTouchableOpacity}
                    onPress={this.changeMoreDetails}>
                    <View style={styles.leftSide}>
                        <View>
                            <Text
                                style={styles.titleStyle}>
                                {this.state.name}
                            </Text>
                        </View>
                        {this.state.showMoreDetails ?
                            <View>
                                <View>
                                    <Text
                                        style={styles.detailStyle}>
                                        An de studiu: {this.state.year}
                                    </Text>
                                </View>
                                <View>
                                    <Text
                                        style={styles.detailStyle}>
                                        Semestru: {this.state.semester}
                                    </Text>
                                </View>
                                <View>
                                    <Text
                                        style={styles.detailStyle}>
                                        Tip: {this.state.type}
                                    </Text>
                                </View>
                                <View>
                                    <Text
                                        style={styles.detailStyle}>
                                        Numar de credite: {this.state.credits}
                                    </Text>
                                </View>
                                <View>
                                    <Text
                                        style={styles.detailStyle}>
                                        Profesor titular curs: {this.state.professor}
                                    </Text>
                                </View>
                                {!(this.state.package===0) ?
                                    <View>
                                    <Text
                                        style={styles.detailStyle}>
                                        Pachet: {this.state.package}
                                    </Text>
                                </View>:null}

                            </View> : null}
                    </View>
                    <View style={styles.rightSide}>
                        <View>
                            {renderIf(this.state.symbol.localeCompare('plus') === 0 ,
                            <TouchableOpacity
                                onPress={this.addCourse}>
                            <View>
                                        <Icon name={this.state.symbol} style={{fontSize: 30, color: '#263238', paddingTop:2,paddingBottom:2,paddingLeft:5,paddingRight:2}}/>
                                </View>
                            </TouchableOpacity>
                            )}
                            {renderIf(this.state.symbol.localeCompare('angle-right') === 0 ,
                                <TouchableOpacity
                                    onPress={this.showGrades}>
                                    <View>
                                        <Icon name={this.state.symbol} style={{fontSize: 30, color: '#263238', paddingTop:2,paddingBottom:2,paddingLeft:5,paddingRight:2}}/>
                                    </View>
                                </TouchableOpacity>
                            )}
                            {renderIf(this.state.symbol.localeCompare('trash') === 0 ,
                                <TouchableOpacity
                                    onPress={this.deleteCourse}>
                                    <View>
                                        <Icon name={this.state.symbol} style={{fontSize: 30, color: '#263238', paddingTop:2,paddingBottom:2,paddingLeft:5,paddingRight:2}}/>

                                    </View>
                                </TouchableOpacity>
                            )}
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
};
const styles = StyleSheet.create({
    container: {
        padding: 5,
    },
    leftSide: {
        flex: 10
    },
    rightSide: {
        justifyContent : 'center',
        flexDirection: 'column',
        flex: 1
    },
    containerTouchableOpacity: {
        flexDirection: 'row',
        padding: 10,
        overflow: 'hidden',
        borderRadius: 1,
        backgroundColor: '#546E7A',
    },
    titleStyle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
    },
    detailStyle: {
        color: '#E0F2F1',
        fontSize: 14,
    },
    showAllDetailsText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 14,
        color: '#cde6ff',
        textDecorationLine: 'underline'
    },
    showLessDetailsText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 14,
        color: '#cde6ff',
        textDecorationLine: 'underline'
    },
});