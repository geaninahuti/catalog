import * as firebase from "firebase";

const config = {
    apiKey: "AIzaSyDLExTC3v_NByNGRMO310k4tKjoMPR5ekw",
    authDomain: "studentcatalog-cebe5.firebaseapp.com",
    databaseURL: "https://studentcatalog-cebe5.firebaseio.com",
    projectId: "studentcatalog-cebe5",
    storageBucket: "",
    messagingSenderId: "132643019625"
};

export default class Firebase {

  static auth;
  static database;

  static init() {
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
      Firebase.auth = firebase.auth();
      Firebase.database = firebase.database();
    }
  }
}