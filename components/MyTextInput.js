import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';


const fumiInput = props => {
    
    return(
        <View style = {styles.container} > 
            <TextInput
                // style = {{backgroundColor:"#f2ccff"}}
                // label={props.labelName}
                // iconClass={FontAwesomeIcon}
                // iconName={props.icon}
                // iconColor={'#602060'}
                // passiveIconColor = {'#602060'}
                // iconSize={20}
                placeholder = {props.placeholder}
                secureTextEntry = {props.secureTextEntry}
                keyboardType = {props.keyboardType}
                ref = {props.ref}
                returnKeyType={props.returnKeyType}
                blurOnSubmit={props.blurOnSubmit}
                onSubmitEditing = {props.onSubmitEditing}    
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      padding: 10,
    },
  });

export default fumiInput